//
//  ManInterstitialController.h
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 18..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#ifndef ManInterstitialController_h
#define ManInterstitialController_h

#import "ManBannerController.h"
#import <WebKit/WebKit.h>

@interface ManInterstitialController : UIViewController <ManInterstitialDelegate>  {

    ManBanner *manInterstitial;

    NSString *publisherID;
    NSString *mediaID;
    NSString *sectionID;
    NSString *viewType;

    CGFloat i_x;
    CGFloat i_y;
    CGFloat i_width;
    CGFloat i_height;

    NSString *appID;
    NSString *appName;
    NSString *storeURL;
}

- (void)setAppID:(NSString*)appID AppName:(NSString*)appName StoreURL:(NSString*)storeURL;

- (void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s viewType:(NSString*)viewtype;

- (void)setSMS:(Boolean)sms setTel:(Boolean)tel setCalendar:(Boolean)calendar setStorePicture:(Boolean)storePicture setInlineVideo:(Boolean)inlineVideo;

- (void)apiData:(Boolean)api_data isAsset:(Boolean)is_asset;

/* 컨텐츠 광고 수신 에러*/
- (void)didFailReceiveAd:(ManBanner*)manContentAdView errorType:(NSInteger)errorType;

@end

#endif /* ManInterstitialController_h */


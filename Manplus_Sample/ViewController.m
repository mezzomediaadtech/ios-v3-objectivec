//
//  ViewController.m
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 5..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setAppInfo];
    [self setVideoOpt];
    
    self.navigationItem.title = @"Manplus";
}

// Set app_id, app_name, stroe_url
- (void)setAppInfo {
    appID = @"appID";
    appName = @"appName";
    storeURL = @"storeURL";
}

// Set Video Option (Only video ad)
/* =================================== */
// Video Ad option
// autoplay : AutoPlay video ad
// autoReplay : Auto-replay video ad
// muted : Play video ad with muted
// clickFull : Set click area to land on ad details page
// viewability : Stop the video if the ad area appears to be less than 20% when scrolling through the page
// closeBtnShow : Close button clears ad area
// soundBtnShow : Button to control sound
// clickBtnShow : Ad click button
// skipBtnShow : Ad skip button
// clickVideoArea : Video area disappears when clicked (true : disappear)
/* =================================== */

- (void)setVideoOpt {
    autoplay = true;
    autoReplay = false;
    muted = false;
    clickFull = true;
    viewability = true;
    closeBtnShow = true;
    soundBtnShow = true;
    clickBtnShow = true;
    skipBtnShow = true;
    clickVideoArea = true;
}

// Ani : Sample App subview loading form

// 띠배너
// (Banner Ad)
// Set Publisher ID, Media ID, Section ID, Xpoint, Ypoint, Banner Width, Banner Height, Target Area Width & Height, Type(banner or interstitial)
/* =================================== */
// Banner Ad view type
// @“0” 띠 배너
// @“1” 전면 배너 : 매체의 영역 소재내 위치
/* =================================== */

- (IBAction)setManBannerAd:(id)sender {
    [self requestManBannerWithPid:@"100" Mid:@"200" Sid:@"300" Ani:YES X:0 Y:0 Width:414 Height:54 Type:@"0"];
}

// 전면배너
// (Interstitial Ad)
// Set Publisher ID, Media ID, Section ID, viewType
/* =================================== */
// Interstitial Ad view style
// @"0" 전체화면
// @“1” 사이즈만 팝업(배경 투명)
// @“2” 사이즈와 팝업(배경 alpha 0.8)
/* =================================== */

- (IBAction)setManInterstitialAd:(id)sender {
    [self requestManInterstitialWithPid:@"100" Mid:@"200" Sid:@"888888" Ani:NO viewType:@"0"];
}

// 동영상
// (Video Ad)
// Set Publisher ID, Media ID, Section ID

- (IBAction)setManVideoAd:(id)sender {
    [self requestManVideoWithPid:@"100" Mid:@"200" Sid:@"888888" Ani:YES X:0 Y:0 Width:414 Height:400];
}

-(void)requestManBannerWithPid:(NSString*)p Mid:(NSString*)m Sid:(NSString*)s Ani:(Boolean)isAni X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h Type:(NSString*)type{
    
    NSLog(@"띠배너광고 호출");
    
    ManBannerController *manBannerController = [[ManBannerController alloc] init];
    [manBannerController setPID:p MID:m SID:s X:x Y:y Width:w Height:h Type:type];
    [manBannerController setAppID:appID AppName:appName StoreURL:storeURL];
    [self.navigationController pushViewController:manBannerController animated:isAni];
}

-(void)requestManInterstitialWithPid:(NSString*)p Mid:(NSString*)m Sid:(NSString*)s Ani:(Boolean)isAni viewType:(NSString*)viewType{

    NSLog(@"전면배너광고 호출");
    
    ManInterstitialController *manInterstitialController = [[ManInterstitialController alloc] init];
    [manInterstitialController setPID:p MID:m SID:s viewType:viewType];
    [manInterstitialController setAppID:appID AppName:appName StoreURL:storeURL];
    [self.navigationController pushViewController:manInterstitialController animated:isAni];
}

-(void)requestManVideoWithPid:(NSString*)p Mid:(NSString*)m Sid:(NSString*)s Ani:(Boolean)isAni X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h {
    
    NSLog(@"동영상광고 호출");
    
    ManVideoController *manVideoController = [[ManVideoController alloc] init];
    [manVideoController setPID:p MID:m SID:s X:x Y:y Width:w Height:h];
    [manVideoController setAppID:appID AppName:appName StoreURL:storeURL];
    [manVideoController setAutoplay:autoplay AutoReplay:autoReplay Muted:muted ClickFull:clickFull CloseBtnShow:closeBtnShow SoundBtnShow:soundBtnShow ClickBtnShow:clickBtnShow SkipBtnShow:skipBtnShow ClickVideoArea:clickVideoArea Viewability:viewability];
    
    // When there are two video areas (광고영역 두개 정의)
    [manVideoController set2PID:@"102" MID:@"202" SID:@"803581" X:0 Y:400 Width:400 Height:300];
    [manVideoController set2AppID:appID AppName:appName StoreURL:storeURL];
    [manVideoController set2Autoplay:autoplay AutoReplay:autoReplay Muted:muted ClickFull:clickFull CloseBtnShow:closeBtnShow SoundBtnShow:soundBtnShow ClickBtnShow:clickBtnShow SkipBtnShow:skipBtnShow ClickVideoArea:clickVideoArea Viewability:viewability];
    
    [self.navigationController pushViewController:manVideoController animated:isAni];
}

@end

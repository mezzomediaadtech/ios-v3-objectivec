//
//  ManBannerController.h
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 10..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#ifndef ManBannerController_h
#define ManBannerController_h

#import "ManBanner.h"
#import <WebKit/WebKit.h>

@interface ManBannerController : UIViewController <ManBannerDelegate, ManInterstitialDelegate> {
    
    ManBanner *manBanner;
    //ManBanner *manInter;
    
    NSString *publisherID;
    NSString *mediaID;
    NSString *sectionID;
    
    CGFloat i_x;
    CGFloat i_y;
    CGFloat i_width;
    CGFloat i_height;
    NSString *bannerType;
    
    NSString *appID;
    NSString *appName;
    NSString *storeURL;

}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (void)setAppID:(NSString*)appID AppName:(NSString*)appName StoreURL:(NSString*)storeURL;

- (void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h Type:(NSString*)type;

- (void)setSMS:(Boolean)sms setTel:(Boolean)tel setCalendar:(Boolean)calendar setStorePicture:(Boolean)storePicture setInlineVideo:(Boolean)inlineVideo;

- (void)apiData:(Boolean)api_data isAsset:(Boolean)is_asset;

/* 컨텐츠 광고 수신 에러*/
- (void)didFailReceiveAd:(ManBanner*)manContentAdView errorType:(NSInteger)errorType;

@end

#endif /* ManBannerController_h */

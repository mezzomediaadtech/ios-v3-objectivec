//
//  ManVideoController.h
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 18..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#ifndef ManVideoController_h
#define ManVideoController_h

#import <WebKit/WebKit.h>
#import "ManVideo.h"

@interface ManVideoController : UIViewController <ManVideoDelegate> {

    ManVideo *manVideo;
    ManVideo *manVideo2;

    
    NSString *publisherID;
    NSString *mediaID;
    NSString *sectionID;
    
    NSString *publisherID2;
    NSString *mediaID2;
    NSString *sectionID2;
    
    CGFloat i_x;
    CGFloat i_y;
    CGFloat i_width;
    CGFloat i_height;
    
    CGFloat i_x2;
    CGFloat i_y2;
    CGFloat i_width2;
    CGFloat i_height2;
    
    NSString *appID;
    NSString *appName;
    NSString *storeURL;
    
    NSString *appID2;
    NSString *appName2;
    NSString *storeURL2;
    
    bool autoplay;     //AutoPlay video ad
    bool autoReplay;   //Auto-replay video ad
    bool muted;        //Play video ad with muted
    bool clickFull;    //Set click area to land on ad details page
    bool viewability;  //Stop the video if the ad area appears to be less than 20% when scrolling through the page
    bool closeBtnShow; //Close button clears ad area
    bool soundBtnShow; //Button to control sound
    bool clickBtnShow; //Ad click button
    bool skipBtnShow;  //Ad skip button
    bool clickVideoArea; //Video area disappears when clicked
}

- (void)setAppID:(NSString*)appID AppName:(NSString*)appName StoreURL:(NSString*)storeURL;

- (void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h;

- (void)setAutoplay:(bool)_autoplay AutoReplay:(bool)_autoReplay Muted:(bool)_muted ClickFull:(bool)_clickFull CloseBtnShow:(bool)_closeBtnShow SoundBtnShow:(bool)_soundBtnShow ClickBtnShow:(bool)_clickBtnShow SkipBtnShow:(bool)_skipBtnShow ClickVideoArea:(bool)_clickVideoArea Viewability:(bool)_viewability;

- (void)set2AppID:(NSString*)appID AppName:(NSString*)appName StoreURL:(NSString*)storeURL;

- (void)set2PID:(NSString*)p MID:(NSString*)m SID:(NSString*)s X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h;

- (void)set2Autoplay:(bool)_autoplay AutoReplay:(bool)_autoReplay Muted:(bool)_muted ClickFull:(bool)_clickFull CloseBtnShow:(bool)_closeBtnShow SoundBtnShow:(bool)_soundBtnShow ClickBtnShow:(bool)_clickBtnShow SkipBtnShow:(bool)_skipBtnShow ClickVideoArea:(bool)_clickVideoArea Viewability:(bool)_viewability;


/* 컨텐츠 광고 수신 에러
 */
- (void)didFailReceiveAd:(ManVideo*)manContentAdView errorType:(NSInteger)errorType;

@end

#endif /* ManVideoController_h */


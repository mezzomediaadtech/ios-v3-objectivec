//
//  ManBannerController.m
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 10..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManBannerController.h"
#import "UIView+Toast.h"

@interface ManBannerController()

@end

@implementation ManBannerController

- (void)viewDidLoad{

    [super viewDidLoad];

    self.title = @"Banner";
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Scroll View 테스트
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.scrollEnabled = YES;
    //scrollView.pagingEnabled = YES;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height * 3);
    [self.view addSubview:scrollView];

    manBanner = [[ManBanner alloc] initWithFrame:CGRectMake(i_x, i_y, i_width, i_height)];

    [manBanner infoSDK];
    
    manBanner.gender = @"2";
    manBanner.age = @"15";
    manBanner.userId = @"mezzomedia";
    manBanner.userEmail = @"mezzo@mezzomeida.co.kr";
    manBanner.userPositionAgree = @"1";
    
    manBanner.backgroundColor = [UIColor whiteColor];
    
    manBanner.bannerDelegate = self;
    
    // u_age_level (0: 어린이, 1: 청소년 및 성인)<필수 Parameter>
    [manBanner userAgeLevel:@"1"];
    
    // app_id, app_name, stroe_url 설정
    [manBanner appID:appID appName:appName storeURL:storeURL];
    
    [manBanner publisherID:publisherID mediaID:mediaID sectionID:sectionID x:i_x y:i_y width:i_width height:i_height type:bannerType];
    
    [manBanner apiData:false isAsset:false];
    
    // Keyword 타게팅을 위한 함수파라미터 (Optional)
    [manBanner keywordParam:@"KeywordTargeting"];
    
    // 추가적인 파라미터 (Optional)
    [manBanner externalParam:@"BannerAdditionalParameters"];
    
    [manBanner startBanner];
    
    [scrollView addSubview:manBanner];
    [self.view addSubview:scrollView];
    
}

-(void) buttonClicked:(UIButton*)sender
{
    [manBanner stopBanner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    NSLog(@"ADBanner ================== didReceiveMemoryWarning");
}

-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h Type:(NSString*)type {
    publisherID = p;
    mediaID = m;
    sectionID = s;
    i_x = x;
    i_y = y;
    i_width = w;
    i_height = h;
    bannerType = type;
    
    NSLog(@"ADBanner ================== Publisher, Media, Section NO");
    NSLog(@"%@", publisherID);
    NSLog(@"%@", mediaID);
    NSLog(@"%@", sectionID);
    NSLog(@"%f", i_x);
    NSLog(@"%f", i_y);
    NSLog(@"%f", i_width);
    NSLog(@"%f", i_height);
    NSLog(@"%@", bannerType);
    NSLog(@"======================================================");
    
}

-(void)setAppID:(NSString*)app_id AppName:(NSString*)app_name StoreURL:(NSString*)store_url{
    appID = app_id;
    appName = app_name;
    storeURL = store_url;
    
    NSLog(@"ADBanner ================== appID, appName, storeURL");
    NSLog(@"%@", appID);
    NSLog(@"%@", appName);
    NSLog(@"%@", storeURL);
    NSLog(@"======================================================");
    
}

- (void)didFailReceiveAd:(ManBanner*)adBanner errorType:(NSInteger)errorType {
    
    // errorType은 ManAdDefine.h 참조
    NSLog(@"[ManBannerAdViewController] =========== didFailReceiveAd : %ld", (long)errorType);
    NSString *log = @"none";
    NSString * temp;

    switch (errorType) {
            
        case NewManAdSuccess:
            log = @"성공";
            //NSLog(@"광고 타입[유료(guarantee) or 무료(house)] : %@", [manBanner getAdType]);
            break;
            
        case NewManAdClick:
            log = @"광고 클릭";
            //[manBanner stopBanner];
            //manBanner = nil;
            break;

        case NewManAdClose:
            log = @"광고 닫기";
            break;
            
        case NewManAdNotError:
            log = @"광고 없음(No Ads)";

            [manBanner stopBanner];
            manBanner = nil;

            break;
            
        case NewManAdPassbackError:
            log = @"Sync 모드 필요 (패스백)";
            break;
            
        case NewManAdTimeoutError:
            log = @"Timeout";
            break;
            
        case NewManAdParsingError:
            log = @"파싱 에러";
            break;
            
        case NewManAdDuplicateError:
            log = @"중복 호출 에러";
            break;
            
        case NewManAdError:
            log = @"에러";
            break;
            
        case NewManBrowserError:
            log = @"Browser Error";
            break;
            
        case NewManAdNotExistError:
            log = @"존재하지 않는 요청 에러";
            break;
            
        case NewManAdAppStoreUrlError:
            log = @"앱 Store URL 미입력";
            break;
            
        case NewManAdIDError:
            log = @"사업자/미디어/섹션 코드 미존재";
            break;
            
        case NewManAdTargetAreaError:
            log = @"광고영역크기 에러";
            break;
            
        case NewManAdUserAgeLevelError:
            log = @"User age level 에러";
            break;
        
        case NewManAdReloadTimeError:
            log = @"광고 재호출(Reload) 에러";
            break;
            
        case NewManAdNetworkError:
            log = @"네트워크 에러";
            break;
            
        case NewManAdFileError:
            log = @"광고물 파일 형식 에러";
            break;
            
        case NewManAdCreativeError:
            log = @"광고물 요청 실패(Timeout)";
            break;
    }
        
    log = [log stringByAppendingString:[NSString stringWithFormat:@" : %ld", (long)errorType]];
    
    [self.navigationController.view makeToast:log];
}

// 지정된 주기 이내에 광고리로드가 발생됨
- (void)didBlockReloadAd:(ManBanner*)adBanner {
    NSLog(@"[ManBannerAdViewController] =========== didBlockReloadAd");
}

@end

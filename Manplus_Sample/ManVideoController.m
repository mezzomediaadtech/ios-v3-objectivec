//
//  ManVideoController.m
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 18..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManVideoController.h"
#import "UIView+Toast.h"

@interface ManVideoController()

@end

@implementation ManVideoController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"Video AD";
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    // Scroll View 선언
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.scrollEnabled = YES;
    //scrollView.pagingEnabled = YES;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height * 3);
    [self.view addSubview:scrollView];

    
    manVideo = [[ManVideo alloc] initWithFrame:CGRectMake(i_x, i_y, i_width, i_height)];
    
    [manVideo infoSDK];

    manVideo.gender = @"2";
    manVideo.age = @"15";
    manVideo.userId = @"mezzomedia";
    manVideo.userEmail = @"mezzo@mezzomeida.co.kr";
    manVideo.userPositionAgree = @"1";
    
    manVideo.videoDelegate = self;
    
    // u_age_level (0: 어린이, 1: 청소년 및 성인)<필수 Parameter>
    [manVideo userAgeLevel:@"1"];

    // app_id, app_name, stroe_url 설정
    [manVideo appID:appID appName:appName storeURL:storeURL];
    
    // 동영상 광고의 옵션 설정
    [manVideo autoplay:autoplay autoReplay:autoReplay muted:muted clickFull:clickFull closeBtnShow:closeBtnShow soundBtnShow:soundBtnShow clickBtnShow:clickBtnShow skipBtnShow:skipBtnShow clickVideoArea:clickVideoArea viewability:viewability];
    
    [manVideo publisherID:publisherID mediaID:mediaID sectionID:sectionID x:i_x y:i_y width:i_width height:i_height];

    // Keyword 타게팅을 위한 함수파라미터 (Optional)
    [manVideo keywordParam:@"KeywordTargeting"];
        
    [manVideo apiData:false isAsset:false];
    
    // 추가적인 파라미터 (Optional)
    [manVideo externalParam:@"VideoAdditionalParameters"];
    
    [scrollView addSubview:manVideo];
    [self.view addSubview:scrollView];
    
    [manVideo startVideo];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    NSLog(@"ADVideo ================== didReceiveMemoryWarning");
}

-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h {
    publisherID = p;
    mediaID = m;
    sectionID = s;
    i_x = x;
    i_y = y;
    i_width = w;
    i_height = h;
    
    NSLog(@"ADVideo ================== Publisher, Media, Section NO");
    NSLog(@"%@", publisherID);
    NSLog(@"%@", mediaID);
    NSLog(@"%@", sectionID);
    NSLog(@"%f", i_x);
    NSLog(@"%f", i_y);
    NSLog(@"%f", i_width);
    NSLog(@"%f", i_height);
    NSLog(@"======================================================");
    
}

-(void)set2PID:(NSString*)p MID:(NSString*)m SID:(NSString*)s X:(CGFloat)x Y:(CGFloat)y Width:(CGFloat)w Height:(CGFloat)h {
    publisherID2 = p;
    mediaID2 = m;
    sectionID2 = s;
    i_x2 = x;
    i_y2 = y;
    i_width2 = w;
    i_height2 = h;
    
    NSLog(@"ADVideo ================== Publisher, Media, Section NO");
    NSLog(@"%@", publisherID2);
    NSLog(@"%@", mediaID2);
    NSLog(@"%@", sectionID2);
    NSLog(@"%f", i_x2);
    NSLog(@"%f", i_y2);
    NSLog(@"%f", i_width2);
    NSLog(@"%f", i_height2);
    NSLog(@"======================================================");
    
}

-(void)setAppID:(NSString*)app_id AppName:(NSString*)app_name StoreURL:(NSString*)store_url{
    appID = app_id;
    appName = app_name;
    storeURL = store_url;
    
    NSLog(@"ADVideo ================== appID, appName, storeURL");
    NSLog(@"%@", appID);
    NSLog(@"%@", appName);
    NSLog(@"%@", storeURL);
    NSLog(@"======================================================");
    
}

-(void)set2AppID:(NSString*)app_id AppName:(NSString*)app_name StoreURL:(NSString*)store_url{
    appID2 = app_id;
    appName2 = app_name;
    storeURL2 = store_url;
    
    NSLog(@"ADVideo ================== appID, appName, storeURL");
    NSLog(@"%@", appID2);
    NSLog(@"%@", appName2);
    NSLog(@"%@", storeURL2);
    NSLog(@"======================================================");
    
}

-(void)setAutoplay:(bool)_autoplay AutoReplay:(bool)_autoReplay Muted:(bool)_muted ClickFull:(bool)_clickFull CloseBtnShow:(bool)_closeBtnShow SoundBtnShow:(bool)_soundBtnShow ClickBtnShow:(bool)_clickBtnShow SkipBtnShow:(bool)_skipBtnShow ClickVideoArea:(bool)_clickVideoArea Viewability:(bool)_viewability {
    autoplay = _autoplay;
    autoReplay = _autoReplay;
    muted = _muted;
    clickFull = _clickFull;
    viewability = _viewability;
    closeBtnShow = _closeBtnShow;
    soundBtnShow = _soundBtnShow;
    clickBtnShow = _clickBtnShow;
    skipBtnShow = _skipBtnShow;
    clickVideoArea = _clickVideoArea;
    
    NSLog(@"ADVideo ================== autoplay, autoReplay, muted, clickFull, closeBtnShow, soundBtnShow, clickBtnShow, skipBtnShow");
    NSLog(@"Autoplay : %@", autoplay ? @"TRUE" : @"FALSE");
    NSLog(@"AutoReplay : %@", autoReplay ? @"TRUE" : @"FALSE");
    NSLog(@"Muted : %@", muted ? @"TRUE" : @"FALSE");
    NSLog(@"ClickFull : %@", clickFull ? @"TRUE" : @"FALSE");
    NSLog(@"Viewability : %@", viewability ? @"TRUE" : @"FALSE");
    NSLog(@"CloseBtnShow : %@", closeBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"SoundBtnShow : %@", soundBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"ClickBtnShow : %@", clickBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"SkipBtnShow : %@", skipBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"ClickVideoArea : %@", clickVideoArea ? @"TRUE" : @"FALSE");
    NSLog(@"======================================================");
    
}

-(void)set2Autoplay:(bool)_autoplay AutoReplay:(bool)_autoReplay Muted:(bool)_muted ClickFull:(bool)_clickFull CloseBtnShow:(bool)_closeBtnShow SoundBtnShow:(bool)_soundBtnShow ClickBtnShow:(bool)_clickBtnShow SkipBtnShow:(bool)_skipBtnShow ClickVideoArea:(bool)_clickVideoArea Viewability:(bool)_viewability{
    autoplay = _autoplay;
    autoReplay = _autoReplay;
    muted = _muted;
    clickFull = _clickFull;
    viewability = _viewability;
    closeBtnShow = _closeBtnShow;
    soundBtnShow = _soundBtnShow;
    clickBtnShow = _clickBtnShow;
    skipBtnShow = _skipBtnShow;
    clickVideoArea = _clickVideoArea;
    
    NSLog(@"ADVideo ================== autoplay, autoReplay, muted, clickFull, closeBtnShow, soundBtnShow, clickBtnShow, skipBtnShow");
    NSLog(@"Autoplay : %@", autoplay ? @"TRUE" : @"FALSE");
    NSLog(@"AutoReplay : %@", autoReplay ? @"TRUE" : @"FALSE");
    NSLog(@"Muted : %@", muted ? @"TRUE" : @"FALSE");
    NSLog(@"ClickFull : %@", clickFull ? @"TRUE" : @"FALSE");
    NSLog(@"Viewability : %@", viewability ? @"TRUE" : @"FALSE");
    NSLog(@"CloseBtnShow : %@", closeBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"SoundBtnShow : %@", soundBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"ClickBtnShow : %@", clickBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"SkipBtnShow : %@", skipBtnShow ? @"TRUE" : @"FALSE");
    NSLog(@"ClickVideoArea : %@", clickVideoArea ? @"TRUE" : @"FALSE");
    NSLog(@"======================================================");
    
}

- (void)didFailReceiveAd:(ManVideo*)adVideo errorType:(NSInteger)errorType {
    
    // errorType은 ManAdDefine.h 참조
    NSLog(@"[ManVideoAdViewController] =========== didFailReceiveAd : %ld", (long)errorType);
    NSString *log = @"none";
    switch (errorType) {
        case NewManAdSuccess:
            log = @"성공";
            //NSLog(@"광고 타입[유료(guarantee) or 무료(house)] : %@", [manVideo getAdType]);
            break;
            
        case NewManAdClick:
            log = @"광고 클릭";
            break;
            
        case NewManAdClose:
            log = @"광고 닫음";
            break;
            
        case NewManVideoAdStart:
            log = @"비디오 광고 시작";
            break;
            
        case NewManVideoAdSkip:
            log = @"비디오 광고 Skip";
            break;
            
        case NewManVideoAdImp:
            log = @"비디오 노출";
            break;
            
        case NewManVideoAdFirstQ:
            log = @"비디오 1/2 재생";
            break;
            
        case NewManVideoAdMidQ:
            log = @"비디오 1/2 재생";
            break;
            
        case NewManVideoAdThirdQ:
            log = @"비디오 3/4 재생";
            break;
            
        case NewManVideoAdComplete:
            log = @"비디오 광고 재생 완료";
            break;
            
        case NewManAdNotError:
            log = @"광고 없음(No Ads)";
            break;
            
        case NewManAdPassbackError:
            log = @"Sync 모드 필요 (패스백)";
            break;
            
        case NewManAdTimeoutError:
            log = @"Timeout";
            break;
            
        case NewManAdParsingError:
            log = @"파싱 에러";
            break;
            
        case NewManAdDuplicateError:
            log = @"중복 호출 에러";
            break;
            
        case NewManAdError:
            log = @"에러";
            break;
            
        case NewManBrowserError:
            log = @"Browser Error";
            break;
            
        case NewManAdNotExistError:
            log = @"존재하지 않는 요청 에러";
            break;
            
        case NewManAdAppStoreUrlError:
            log = @"앱 Store URL 미입력";
            break;
            
        case NewManAdVideoOptError:
            log = @"비디오 옵션 설정";
            break;
            
        case NewManAdTargetAreaError:
            log = @"광고영역크기 에러";
            break;
            
        case NewManAdUserAgeLevelError:
            log = @"User age level 에러";
            break;
            
        case NewManAdIDError:
            log = @"사업자/미디어/섹션 코드 미존재";
            break;
            
        case NewManAdReloadTimeError:
            log = @"광고 재호출(Reload) 에러";
            break;
            
        case NewManAdNetworkError:
            log = @"네트워크 에러";
            break;
            
        case NewManAdCreativeError:
            log = @"광고물 요청 실패(Timeout)";
            break;
    }
    
    log = [log stringByAppendingString:[NSString stringWithFormat:@" : %ld", (long)errorType]];
    
    [self.navigationController.view makeToast:log];
}

// 지정된 주기 이내에 광고리로드가 발생됨
- (void)didBlockReloadAd:(ManVideo*)adBanner {
    NSLog(@"[ManVideoAdViewController] =========== didBlockReloadAd");
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        NSLog(@"PORTRAIT");
        CGRect frame = manVideo.bounds;
        frame.origin.x = i_x;
        frame.origin.y = i_y;
        frame.size.width = i_width;
        frame.size.height = i_height;
        manVideo.bounds = frame;
        manVideo.frame = frame;
    }
    
    else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
    {
        NSLog(@"LANDSCAPE");
        CGRect screen = [[UIScreen mainScreen] bounds];
        CGRect frame = manVideo.bounds;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = screen.size.height;
        frame.size.height = screen.size.width;
        manVideo.bounds = frame;
        manVideo.frame = frame;
    }
}

@end

//
//  ManInterstitialController.m
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 18..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManInterstitialController.h"
#import "UIView+Toast.h"

@interface ManInterstitialController()

@end

@implementation ManInterstitialController

// Hide or Don't Hide Status Bar
- (BOOL)prefersStatusBarHidden {
    
    return YES;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"Interstitial Banner";
    self.view.backgroundColor = [UIColor whiteColor];
    
    manInterstitial = [[ManBanner alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height)];
        
    [manInterstitial infoSDK];

    manInterstitial.gender = @"2";
    manInterstitial.age = @"15";
    manInterstitial.userId = @"mezzomedia";
    manInterstitial.userEmail = @"mezzo@mezzomeida.co.kr";
    manInterstitial.userPositionAgree = @"1";
    
    manInterstitial.interDelegate = self;
    
    // u_age_level (0: 어린이, 1: 청소년 및 성인)<필수 Parameter>
    [manInterstitial userAgeLevel:@"1"];
    
    // app_id, app_name, stroe_url 설정
    [manInterstitial appID:appID appName:appName storeURL:storeURL];
    
    [manInterstitial publisherID:publisherID mediaID:mediaID sectionID:sectionID viewType:viewType];
    
    [manInterstitial apiData:false isAsset:false];
    
    // Keyword 타게팅을 위한 함수파라미터 (Optional)
    [manInterstitial keywordParam:@"KeywordTargeting"];
    
    // 추가적인 파라미터 (Optional)
    [manInterstitial externalParam:@"InterstitialAdditionalParameters"];
    
    [self.view addSubview:manInterstitial];
    
    [manInterstitial startInterstitial];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    NSLog(@"ADInterstitial ================== didReceiveMemoryWarning");
}

-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s viewType:(NSString*)viewtype{
    publisherID = p;
    mediaID = m;
    sectionID = s;
    viewType = viewtype;
    
    NSLog(@"ADInterstitial ================== Publisher, Media, Section NO");
    NSLog(@"%@", publisherID);
    NSLog(@"%@", mediaID);
    NSLog(@"%@", sectionID);
    NSLog(@"%@", viewType);
    NSLog(@"======================================================");
    
}

-(void)setAppID:(NSString*)app_id AppName:(NSString*)app_name StoreURL:(NSString*)store_url{
    appID = app_id;
    appName = app_name;
    storeURL = store_url;
    
    NSLog(@"ADInterstitial ================== appID, appName, storeURL");
    NSLog(@"%@", appID);
    NSLog(@"%@", appName);
    NSLog(@"%@", storeURL);
    NSLog(@"======================================================");
    
}

- (void)didFailReceiveAd:(ManBanner*)adBanner errorType:(NSInteger)errorType {
    
    // errorType은 ManAdDefine.h 참조
    NSLog(@"[ManInterstitialAdViewController]=========== didFailReceiveInterstitial : errorType : %ld", (long)errorType);
    NSString *log = @"none";
    switch (errorType) {
        case NewManAdSuccess:
            log = @"성공";
            //NSLog(@"광고 타입[유료(guarantee) or 무료(house)] : %@", [manInterstitial getAdType]);
            break;
            
        case NewManAdClick:
            log = @"광고 클릭";
            break;
            
        case NewManAdClose:
            log = @"광고 닫음";
            break;
            
        case NewManAdNotError:
            log = @"광고 없음(No Ads)";
            break;
            
        case NewManAdPassbackError:
            log = @"Sync 모드 필요 (패스백)";
            break;
            
        case NewManAdTimeoutError:
            log = @"Timeout";
            break;
            
        case NewManAdParsingError:
            log = @"파싱 에러";
            break;
            
        case NewManAdDuplicateError:
            log = @"중복 호출 에러";
            break;
            
        case NewManAdError:
            log = @"에러";
            break;
            
        case NewManBrowserError:
            log = @"Browser Error";
            break;
            
        case NewManAdNotExistError:
            log = @"존재하지 않는 요청 에러";
            break;
            
        case NewManAdAppStoreUrlError:
            log = @"앱 Store URL 미입력";
            break;
            
        case NewManAdIDError:
            log = @"사업자/미디어/섹션 코드 미존재";
            break;
            
        case NewManAdTargetAreaError:
            log = @"광고영역크기 에러";
            break;
       
        case NewManAdUserAgeLevelError:
            log = @"User age level 에러";
            break;
            
        case NewManAdReloadTimeError:
            log = @"광고 재호출(Reload) 에러";
            break;
            
        case NewManAdNetworkError:
            log = @"네트워크 에러";
            break;
            
        case NewManAdFileError:
            log = @"광고물 파일 형식 에러";
            break;
            
        case NewManAdCreativeError:
            log = @"광고물 요청 실패(Timeout)";
            break;
    }
    
    log = [log stringByAppendingString:[NSString stringWithFormat:@" : %ld", (long)errorType]];
    
    [self.navigationController.view makeToast:log];
}


- (void)didClickInterstitial {
    // Click AD
    NSLog(@"[ManInterstitialAdViewController]=========== Click Interstitial!");
}

- (void)didCloseInterstitial {
    // Close AD
    NSLog(@"[ManInterstitialAdViewController]=========== Close Interstitial!");
}

// 지정된 주기 이내에 광고리로드가 발생됨
- (void)didBlockReloadAd:(ManBanner*)adBanner {
    NSLog(@"[ManBannerAdViewController] =========== didBlockReloadAd");
}

@end

//
//  ManAdDefine.h
//  Manplus
//
//  Created by MezzoMedia on 2019. 5. 17..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#ifndef ManAdDefine_h
#define ManAdDefine_h

/* 광고 수신 에러 2.0 */
typedef enum {
    
    NewManAdSuccess            = 200,       // 성공
    NewManAdClick              = 201,       // 광고 클릭
    NewManAdClose              = 202,       // 광고 닫기
    NewManVideoAdStart         = 301,       // 비디오 광고 시작
    NewManVideoAdSkip          = 302,       // 비디오 광고 Skip
    NewManVideoAdImp           = 303,       // 비디오 노출
    NewManVideoAdFirstQ        = 304,       // 비디오 1/4 재생
    NewManVideoAdMidQ          = 305,       // 비디오 1/2 재생
    NewManVideoAdThirdQ        = 306,       // 비디오 3/4 재생
    NewManVideoAdComplete      = 309,       // 비디오 광고 재생완료
    
    NewManAdNotError           = 404,       // 광고 없음 (No Ads)
    NewManAdPassbackError      = 405,       // Sync 모드 필요 (패스백)
    NewManAdTimeoutError       = 408,       // Timeout
    NewManAdParsingError       = 415,       // Parsing Error
    NewManAdDuplicateError     = 498,       // Duplicate Request Error
    NewManAdError              = 499,       // Error
    NewManBrowserError         = 501,       // Browser Error
    
    //NewManAdRequestError       = -1002,     // 잘못된 광고 요청
    NewManAdNotExistError      = -2001,        // Webview 페이지 경로 Error
    NewManAdAppStoreUrlError   = -3001,       // 매체측 앱 스토어 URL
    NewManAdIDError            = -3002,       // 광고 솔루션에서 발급 한 사업자/미디어/섹션 코드 미존재
    NewManAdTargetAreaError    = -3003,        // 광고 영역 크기 에러
    NewManAdVideoOptError      = -3004,        // 비디오 옵션 에러
    NewManAdUserAgeLevelError  = -3005,        // User Age Level 에러
    NewManAdReloadTimeError    = -5002,        // 광고 재호출(Reload) 에러
    NewManAdNetworkError       = -6002,        // 네트워크 에러
    NewManAdFileError          = -8001,        // 광고물 파일 형식 에러
    NewManAdCreativeError      = -9001         // 광고물 요청 실패 (Timeout)
} NewManAdErrorType;

#endif

//
//  ManBanner.h
//  Manplus_v200
//
//  Created by MezzoMedia on 2019. 5. 28..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#ifndef ManBanner_h
#define ManBanner_h

#import <UIKit/UIKit.h>
#import "ManAdDefine.h"
#import "ManViewability.h"

#endif /* ManBanner_h */

@protocol ManBannerDelegate;
@protocol ManInterstitialDelegate;

@interface ManBanner : UIView{
    NSMutableData *responseData;
    
}

@property (nonatomic, strong) ManViewability* viewable;

/* 전달받는 뷰컨트롤러의 객체
 */
@property (nonatomic, assign) id<ManBannerDelegate>bannerDelegate;
@property (nonatomic, assign) id<ManInterstitialDelegate>interDelegate;

/* 광고를 사용하는 유저의 성별 정보
 남성: @"1"
 여성: @"2"
 */
@property (nonatomic, copy) NSString *gender;

/* 광고를 사용하는 유저의 나이 정보
 age : @"20"
 */
@property (nonatomic, copy) NSString *age;

/* 광고를 사용하는 유저의 매체ID
 userId : @"userId"
 */
@property (nonatomic, copy) NSString *userId;

/* 광고를 사용하는 유저의 매체ID
 userEmail : @"user@mezzomedia.co.kr"
 */
@property (nonatomic, copy) NSString *userEmail;

/* 광고를 사용하는 유저의 위치정보 제공 동의여부
 활용 미동의 : @"0"
 활용 동의 : @"1"
 */
@property (nonatomic, copy) NSString *userPositionAgree;

// Set app_id, app_name, stroe_url
- (void)appID:(NSString*)appID appName:(NSString*)appName storeURL:(NSString*)storeURL;

- (void)setSMS:(Boolean)sms setTel:(Boolean)tel setCalendar:(Boolean)calendar setStorePicture:(Boolean)storePicture setInlineVideo:(Boolean)inlineVideo;

- (void)apiData:(Boolean)api_data isAsset:(Boolean)is_asset;

// publisher ID, media ID, section ID, xPoint, yPoint, i_banner_w, i_banner_h, bannerType
- (void)publisherID:(NSString*)publisherID mediaID:(NSString*)mediaID sectionID:(NSString*)sectionID x:(CGFloat)x y:(CGFloat)y width:(CGFloat)w height:(CGFloat)h type:(NSString*)bannerType;

// publisher ID, media ID, section ID, viewType (Interstitial)
- (void)publisherID:(NSString*)publisherID mediaID:(NSString*)mediaID sectionID:(NSString*)sectionID viewType:(NSString*)viewtype;

// keyword parameter
- (void)keywordParam:(NSString*)param;

// external parameter
- (void)externalParam:(NSString*)param;

- (void)userAgeLevel:(NSString*)userAgeLevel;

- (void)startBanner;

- (void)stopBanner;

- (void)startInterstitial;

- (void)stopInterstitial;

- (void)infoSDK;

- (NSString *)getAdType;

- (NSString *)getNativeResponse;

@end

/* 배너 프로토콜
 */
@protocol ManBannerDelegate <NSObject>



/* 배너 광고 수신 에러 */
- (void)didFailReceiveAd:(ManBanner*)adBanner errorType:(NSInteger)errorType;

/* 배너 부정 재요청 (지정된 시간 이내에 재요청이 발생함) */
- (void)didBlockReloadAd:(ManBanner*)adBanner;

//- (NSMutableDictionary*)parseJsonData:(NSData*)adInfoData;

@end

@protocol ManInterstitialDelegate <NSObject>

- (void)didFailReceiveAd:(ManBanner*)adBanner errorType:(NSInteger)errorType;

/* 배너 부정 재요청 (지정된 시간 이내에 재요청이 발생함) */
- (void)didBlockReloadAd:(ManBanner*)adBanner;

@end

//
//  ManViewability.h
//  Manplus_v200
//
//  Created by MezzoMedia on 2020/09/04.
//  Copyright © 2020 MezzoMedia. All rights reserved.
//

#ifndef ManViewability_h
#define ManViewability_h

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ManViewability : NSObject

+ (void)startViewability:(WKWebView *)_view time:(NSString*)t y:(CGFloat*)_y direction:(BOOL)_d;
+ (void)startViewabilityTimer;
+ (void)stopViewabilityTimer;
+ (void)startCheckViewableTimer;
+ (void)stopCheckViewableTimer;
+ (void)timerFired:(NSTimer *)timer;
+ (void)checkScroll:(NSTimer *)timer;
+ (Boolean)isVisible:(WKWebView *)view;
+ (float)calViewablePer:(WKWebView *)view;

@end

#endif /* ManViewability_h */

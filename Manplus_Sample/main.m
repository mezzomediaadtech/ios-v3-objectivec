//
//  main.m
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 5..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
